### Demo Angular
Proyecto CRUD consumiendo una API en https://adsy-system.com/

------------
##   **Tecnología:**
- Angular Cli 11.
-  Api http://sistdemoapi.adsy-system.com.
-  Boostrap 4.4.
- Template: AdminLTE 3.0.5.

------------
## Ejecución Local:
- #### Instalar Node Js:
	- ##### Windows o Mac:
		- Abre la dirección web https://nodejs.org.
		- La pagina te mostrara automáticamente la opción de descargar la versión para Windows o Mac.
		- Descarga el instalador en tu escritorio o el directorio que desees.
		- Ejecuta el instalador y sigue cada uno de los pasos.
	- ##### Linux:
		- Abrir el terminal y ejecutar lo siguiente:
            - sudo apt update.
            - sudo apt install nodejs.
            - sudo apt install npm.

- #### Instalar Angular CLI:
	- ##### Windows:
		- Buscar y abrir la consola de Node.js y ejecutar lo siguiente:
				- npm install -g @angular/cli.
	- ##### Linux:
		- Abrir el terminal y ejecutar lo siguiente:
			- sudo apt update.
			- sudo npm install -g @angular/cli.
	- ##### Mac:
		- Abrir el terminal y ejecutar lo siguiente:
			- sudo apt update.
			- npm install -g @angular/cli.

- #### Clonar el Proyecto.

- ####  Ejecución del Proyecto:
	- Abrir el Terminal de su preferencia y ejecutar lo siguiente:
		- ng build.
		- ng serve.
