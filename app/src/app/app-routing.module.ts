import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AfiliadosgruposComponent } from './afiliadosgrupos/afiliadosgrupos.component';
import { HomeComponent } from './home/home.component';
import { AnunciantegruposComponent } from './anunciantegrupos/anunciantegrupos.component';
import { ProductosgruposComponent } from './productosgrupos/productosgrupos.component';
import { ProveedoresgruposComponent } from './proveedoresgrupos/proveedoresgrupos.component';

const routes: Routes = [
  { path: '', component: HomeComponent,pathMatch: 'full' },
  { path: 'afiliadosgrupos', component: AfiliadosgruposComponent },
  { path: 'anunciantegrupos', component: AnunciantegruposComponent },
  { path: 'productogrupos', component: ProductosgruposComponent },
  { path: 'proveedoresgrupos', component: ProveedoresgruposComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
