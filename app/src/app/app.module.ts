import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AfiliadosgruposComponent } from './afiliadosgrupos/afiliadosgrupos.component';
import { HomeComponent } from './home/home.component';
import { LeftMenuComponent } from './left-menu/left-menu.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { ListAfiliadosgruposComponent } from './afiliadosgrupos/list-afiliadosgrupos/list-afiliadosgrupos.component';
import { FormAfiliadosgruposComponent } from './afiliadosgrupos/form-afiliadosgrupos/form-afiliadosgrupos.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap' ;
import { FormsModule } from '@angular/forms';
import { AnunciantegruposComponent } from './anunciantegrupos/anunciantegrupos.component';
import { ListAnunciantegruposComponent } from './anunciantegrupos/list-anunciantegrupos/list-anunciantegrupos.component';
import { FormAnunciantegruposComponent } from './anunciantegrupos/form-anunciantegrupos/form-anunciantegrupos.component';
import { ProductosgruposComponent } from './productosgrupos/productosgrupos.component';
import { ListProductosgruposComponent } from './productosgrupos/list-productosgrupos/list-productosgrupos.component';
import { FormProductosgruposComponent } from './productosgrupos/form-productosgrupos/form-productosgrupos.component';
import { ProveedoresgruposComponent } from './proveedoresgrupos/proveedoresgrupos.component';
import { ListProveedoresgruposComponent } from './proveedoresgrupos/list-proveedoresgrupos/list-proveedoresgrupos.component';
import { FormProveedoresgruposComponent } from './proveedoresgrupos/form-proveedoresgrupos/form-proveedoresgrupos.component';

@NgModule({
  declarations: [
    AppComponent,
    AfiliadosgruposComponent,
    HomeComponent,
    LeftMenuComponent,
    HeaderComponent,
    FooterComponent,
    ListAfiliadosgruposComponent,
    FormAfiliadosgruposComponent,
    AnunciantegruposComponent,
    ListAnunciantegruposComponent,
    FormAnunciantegruposComponent,
    ProductosgruposComponent,
    ListProductosgruposComponent,
    FormProductosgruposComponent,
    ProveedoresgruposComponent,
    ListProveedoresgruposComponent,
    FormProveedoresgruposComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
