import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAnunciantegruposComponent } from './list-anunciantegrupos.component';

describe('ListAnunciantegruposComponent', () => {
  let component: ListAnunciantegruposComponent;
  let fixture: ComponentFixture<ListAnunciantegruposComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListAnunciantegruposComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAnunciantegruposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
