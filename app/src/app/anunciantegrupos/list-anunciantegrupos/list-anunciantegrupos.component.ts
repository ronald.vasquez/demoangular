import { Component, OnInit } from '@angular/core';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { AnuncianteGrupos } from 'src/app/models/anunciantegrupos';
import { ApianunciantegruposService } from 'src/app/services/apianunciantegrupos.service';

@Component({
  selector: 'app-list-anunciantegrupos',
  templateUrl: './list-anunciantegrupos.component.html',
  styleUrls: ['./list-anunciantegrupos.component.scss'],
  providers: [NgbPaginationConfig]
})
export class ListAnunciantegruposComponent implements OnInit {

  constructor(
    public apiresponse:ApianunciantegruposService,
    private config: NgbPaginationConfig,
    private toastr: ToastrService
  ) { 
    this.config.boundaryLinks = true;
  }

  ngOnInit(): void {
    this.apiresponse.pagination.page =1;
	  this.apiresponse.pagination.previouspage =1;
    this.apiresponse.pagination.countregistre=10;
    this.apiresponse.get(this.apiresponse.pagination.page,this.apiresponse.pagination.countregistre);
  }
  loadPage(page: number) {
    if (page !== this.apiresponse.pagination.previouspage) {
      this.apiresponse.pagination.previouspage = page;
      this.apiresponse.get(this.apiresponse.pagination.page,this.apiresponse.pagination.countregistre);
    }
  }
  selection(){
    this.apiresponse.pagination.countregistre =  parseInt(this.apiresponse.optionselection);
    this.apiresponse.get(this.apiresponse.pagination.page,this.apiresponse.pagination.countregistre);
  }
  update(request: AnuncianteGrupos){
    this.apiresponse.update(request);
  }
  delete(id?:number){
    if(confirm("Esta seguro que desea eliminar el registro")){
        this.apiresponse.delete(id).subscribe(response=>{
        if(response.isSuccess){
          this.apiresponse.pagination.page =1;
	        this.apiresponse.pagination.previouspage =1;
          this.apiresponse.pagination.countregistre=10;
          this.apiresponse.get(this.apiresponse.pagination.page,this.apiresponse.pagination.countregistre);
          this.apiresponse.optionselection="10";
        }
        else{
          this.toastr.error(response.internalErrorMessage,"Anunciante Grupo");
        }
      });
    }
  }

}
