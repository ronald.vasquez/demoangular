import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AnuncianteGrupos } from 'src/app/models/anunciantegrupos';
import { ApianunciantegruposService } from 'src/app/services/apianunciantegrupos.service';

@Component({
  selector: 'app-form-anunciantegrupos',
  templateUrl: './form-anunciantegrupos.component.html',
  styleUrls: ['./form-anunciantegrupos.component.scss']
})
export class FormAnunciantegruposComponent implements OnInit,OnDestroy {

  form: FormGroup;
  subscription:Subscription | undefined;
  datarequest:AnuncianteGrupos | undefined;
  idrequest:number | undefined;
  constructor(private formBuilder:FormBuilder,private _api: ApianunciantegruposService,
    private toastr: ToastrService) { 
    this.form=this.formBuilder.group({
      id:0,
      nombre:['',[Validators.required,Validators.maxLength(50)]],
      estatus:true
    });
  }
  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.subscription=this._api.getdata().subscribe(response=>{
      this.datarequest=response as AnuncianteGrupos;
      this.form.patchValue({
        nombre: this.datarequest.nombre,
        estatus:this.datarequest.visible
      });
      this.idrequest=this.datarequest.id;
    });
  }
  save(){
    if(this.idrequest===undefined || this.idrequest===0){
      this.insert();
    }else{
      this.updateform();
    }
  }

  insert(){
    const fvalue: AnuncianteGrupos={
      nombre: this.form.get('nombre')?.value,
      visible: this.form.get('estatus')?.value,
      iduser:0,
      codigoempresa:"001"
    }
    this._api.save(fvalue).subscribe(response=> {
      if(response.isSuccess){
        this.toastr.success("Registro agregado","Anunciante grupo");
        this._api.pagination.page =1;
	      this._api.pagination.previouspage =1;
        this._api.pagination.countregistre=10;
        this._api.optionselection="10";
        this._api.get(this._api.pagination.page,this._api.pagination.countregistre);
        this.form.reset();
      }
      else{
        this.toastr.error(response.internalErrorMessage,"Anunciante grupo");
      }
    });
  }
  updateform(){
    const fvalue: AnuncianteGrupos={
      id:this.datarequest?.id,
      nombre: this.form.get('nombre')?.value,
      visible: this.form.get('estatus')?.value,
      iduser:0,
      codigoempresa:"001"
    }
    this._api.updatedata(fvalue,this.datarequest?.id).subscribe(response=>{
      if(response.isSuccess){
        this.toastr.info("Registro actualizado","Anunciante grupo");
        this._api.pagination.page =1;
	      this._api.pagination.previouspage =1;
        this._api.pagination.countregistre=10;
        this._api.optionselection="10";
        this._api.get(this._api.pagination.page,this._api.pagination.countregistre);
        this.form.reset();
        this.idrequest=0;
      }
      else{
        this.toastr.error(response.internalErrorMessage,"Anunciante grupo");
      }
    });
  }

}
