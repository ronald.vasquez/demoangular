import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAnunciantegruposComponent } from './form-anunciantegrupos.component';

describe('FormAnunciantegruposComponent', () => {
  let component: FormAnunciantegruposComponent;
  let fixture: ComponentFixture<FormAnunciantegruposComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormAnunciantegruposComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAnunciantegruposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
