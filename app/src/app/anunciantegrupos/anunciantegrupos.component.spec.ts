import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnunciantegruposComponent } from './anunciantegrupos.component';

describe('AnunciantegruposComponent', () => {
  let component: AnunciantegruposComponent;
  let fixture: ComponentFixture<AnunciantegruposComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnunciantegruposComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnunciantegruposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
