import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable } from 'rxjs';
import { Pagination } from '../models/pagination';
import { ProductosGrupos } from '../models/productosgrupos';
import { Response } from '../models/response';

@Injectable({
  providedIn: 'root'
})
export class ApiproductosgruposService {

  public lts: ProductosGrupos[] | undefined;
  public pagination= new Pagination();
  public optionselection: string="10";
  url: string="http://sistdemoapi.adsy-system.com/api/ProductosGrupos/";
  response: Response | undefined;
  paramquery:string | undefined;
  constructor(
    private _httpcli: HttpClient,
    private toastr: ToastrService
  ) { }
  private updateform= new BehaviorSubject<ProductosGrupos>({} as any);
  get(page:number,countreg:number){
    if(page>0){
      this.paramquery="?Pagina="+page;
    }
    if(countreg>0){
      if(this.paramquery!=""){
        this.paramquery=this.paramquery+"&CantidadRegistrosPorPagina="+countreg;
      }
    }
    else{
      this.paramquery="";
    }
    this._httpcli.get(this.url+this.paramquery).toPromise()
    .then(response=>{
      this.response=response as Response;
      if(this.response.isSuccess){
        this.lts=this.response.body as ProductosGrupos[];
        this.pagination.totalcount=this.response.totalcount;
        this.pagination.totalpage=this.response.totalpage;
        this.pagination.countpage=this.pagination.countregistre*this.pagination.page;
        if(this.pagination.totalcount<this.pagination.countpage)
        {
         this.pagination.countpage=this.pagination.totalcount
        }
      }
      else{
        this.toastr.error(this.response.internalErrorMessage,"Productos Grupo");
       this.pagination.page=1;
       this.pagination.previouspage=1;
       this.pagination.totalpage=0;
       this.pagination.countregistre=0;
       this.pagination.showpagination = false;
      }
    })
    return this._httpcli.get<Response>(this.url);
  }
  getdata(): Observable<ProductosGrupos>{
    return this.updateform.asObservable();
  }
  save(request:ProductosGrupos): Observable<Response>{
    return this._httpcli.post<Response>(this.url,request);
  }
  update(request:ProductosGrupos){
    this.updateform.next(request);
  }
  updatedata(request:ProductosGrupos,id?: number): Observable<Response>{
    return this._httpcli.put<Response>(this.url+id,request);
  }
  delete(id?:number):Observable<Response>{
    return this._httpcli.delete<Response>(this.url+id);
  }
}
