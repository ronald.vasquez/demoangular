import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable } from 'rxjs';
import { AnuncianteGrupos } from '../models/anunciantegrupos';
import { Pagination } from '../models/pagination';
import { Response } from '../models/response';

@Injectable({
  providedIn: 'root'
})
export class ApianunciantegruposService {
  public lts: AnuncianteGrupos[] | undefined;
  public pagination= new Pagination();
  public optionselection: string="10";
  url: string="http://sistdemoapi.adsy-system.com/api/AnuncianteGrupos/";
  response: Response | undefined;
  paramquery:string | undefined;
  constructor(
    private _httpcli: HttpClient,
    private toastr: ToastrService
  ) { }
  private updateform= new BehaviorSubject<AnuncianteGrupos>({} as any);
  get(page:number,countreg:number){
    if(page>0){
      this.paramquery="?Pagina="+page;
    }
    if(countreg>0){
      if(this.paramquery!=""){
        this.paramquery=this.paramquery+"&CantidadRegistrosPorPagina="+countreg;
      }
    }
    else{
      this.paramquery="";
    }
    this._httpcli.get(this.url+this.paramquery).toPromise()
    .then(response=>{
      this.response=response as Response;
      if(this.response.isSuccess){
        this.lts=this.response.body as AnuncianteGrupos[];
        this.pagination.totalcount=this.response.totalcount;
        this.pagination.totalpage=this.response.totalpage;
        this.pagination.countpage=this.pagination.countregistre*this.pagination.page;
        if(this.pagination.totalcount<this.pagination.countpage)
        {
         this.pagination.countpage=this.pagination.totalcount
        }
      }
      else{
        this.toastr.error(this.response.internalErrorMessage,"Anunciante Grupo");
       this.pagination.page=1;
       this.pagination.previouspage=1;
       this.pagination.totalpage=0;
       this.pagination.countregistre=0;
       this.pagination.showpagination = false;
      }
    })
    return this._httpcli.get<Response>(this.url);
  }
  getdata(): Observable<AnuncianteGrupos>{
    return this.updateform.asObservable();
  }
  save(request:AnuncianteGrupos): Observable<Response>{
    return this._httpcli.post<Response>(this.url,request);
  }
  update(request:AnuncianteGrupos){
    this.updateform.next(request);
  }
  updatedata(request:AnuncianteGrupos,id?: number): Observable<Response>{
    return this._httpcli.put<Response>(this.url+id,request);
  }
  delete(id?:number):Observable<Response>{
    return this._httpcli.delete<Response>(this.url+id);
  }
}
