import { Component, OnInit } from '@angular/core';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ProveedoresGrupos } from 'src/app/models/proveedoresgrupos';
import { ApiproductosgruposService } from 'src/app/services/apiproductosgrupos.service';
import { ApiproveedoresgruposService } from 'src/app/services/apiproveedoresgrupos.service';

@Component({
  selector: 'app-list-proveedoresgrupos',
  templateUrl: './list-proveedoresgrupos.component.html',
  styleUrls: ['./list-proveedoresgrupos.component.scss']
})
export class ListProveedoresgruposComponent implements OnInit {
  constructor(
    public apiresponse:ApiproveedoresgruposService,
    private config: NgbPaginationConfig,
    private toastr: ToastrService
  ) { 
    this.config.boundaryLinks = true;
  }

  ngOnInit(): void {
    this.apiresponse.pagination.page =1;
	  this.apiresponse.pagination.previouspage =1;
    this.apiresponse.pagination.countregistre=10;
    this.apiresponse.get(this.apiresponse.pagination.page,this.apiresponse.pagination.countregistre);
  }
  loadPage(page: number) {
    if (page !== this.apiresponse.pagination.previouspage) {
      this.apiresponse.pagination.previouspage = page;
      this.apiresponse.get(this.apiresponse.pagination.page,this.apiresponse.pagination.countregistre);
    }
  }
  selection(){
    this.apiresponse.pagination.countregistre =  parseInt(this.apiresponse.optionselection);
    this.apiresponse.get(this.apiresponse.pagination.page,this.apiresponse.pagination.countregistre);
  }
  update(request: ProveedoresGrupos){
    this.apiresponse.update(request);
  }
  delete(id?:number){
    if(confirm("Esta seguro que desea eliminar el registro")){
        this.apiresponse.delete(id).subscribe(response=>{
        if(response.isSuccess){
          this.apiresponse.pagination.page =1;
	        this.apiresponse.pagination.previouspage =1;
          this.apiresponse.pagination.countregistre=10;
          this.apiresponse.get(this.apiresponse.pagination.page,this.apiresponse.pagination.countregistre);
          this.apiresponse.optionselection="10";
        }
        else{
          this.toastr.error(response.internalErrorMessage,"Proveedores Grupo");
        }
      });
    }
  }

}
