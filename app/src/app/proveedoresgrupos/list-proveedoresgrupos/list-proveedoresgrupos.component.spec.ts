import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListProveedoresgruposComponent } from './list-proveedoresgrupos.component';

describe('ListProveedoresgruposComponent', () => {
  let component: ListProveedoresgruposComponent;
  let fixture: ComponentFixture<ListProveedoresgruposComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListProveedoresgruposComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProveedoresgruposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
