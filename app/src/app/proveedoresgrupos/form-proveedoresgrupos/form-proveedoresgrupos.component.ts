import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { ProveedoresGrupos } from 'src/app/models/proveedoresgrupos';
import { ApiproveedoresgruposService } from 'src/app/services/apiproveedoresgrupos.service';

@Component({
  selector: 'app-form-proveedoresgrupos',
  templateUrl: './form-proveedoresgrupos.component.html',
  styleUrls: ['./form-proveedoresgrupos.component.scss']
})
export class FormProveedoresgruposComponent implements OnInit {

  form: FormGroup;
  subscription:Subscription | undefined;
  datarequest:ProveedoresGrupos | undefined;
  idrequest:number | undefined;
  constructor(private formBuilder:FormBuilder,private _api: ApiproveedoresgruposService,
    private toastr: ToastrService) { 
    this.form=this.formBuilder.group({
      id:0,
      nombre:['',[Validators.required,Validators.maxLength(50)]],
      estatus:true
    });
  }
  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.subscription=this._api.getdata().subscribe(response=>{
      this.datarequest=response as ProveedoresGrupos;
      this.form.patchValue({
        nombre: this.datarequest.nombre,
        estatus:this.datarequest.visible
      });
      this.idrequest=this.datarequest.id;
    });
  }
  save(){
    if(this.idrequest===undefined || this.idrequest===0){
      this.insert();
    }else{
      this.updateform();
    }
  }

  insert(){
    const fvalue: ProveedoresGrupos={
      nombre: this.form.get('nombre')?.value,
      visible: this.form.get('estatus')?.value,
      iduser:0,
      codigoempresa:"001"
    }
    this._api.save(fvalue).subscribe(response=> {
      if(response.isSuccess){
        this.toastr.success("Registro agregado","Proveedores grupo");
        this._api.pagination.page =1;
	      this._api.pagination.previouspage =1;
        this._api.pagination.countregistre=10;
        this._api.optionselection="10";
        this._api.get(this._api.pagination.page,this._api.pagination.countregistre);
        this.form.reset();
      }
      else{
        this.toastr.error(response.internalErrorMessage,"Proveedores grupo");
      }
    });
  }
  updateform(){
    const fvalue: ProveedoresGrupos={
      id:this.datarequest?.id,
      nombre: this.form.get('nombre')?.value,
      visible: this.form.get('estatus')?.value,
      iduser:0,
      codigoempresa:"001"
    }
    this._api.updatedata(fvalue,this.datarequest?.id).subscribe(response=>{
      if(response.isSuccess){
        this.toastr.info("Registro actualizado","Proveedores grupo");
        this._api.pagination.page =1;
	      this._api.pagination.previouspage =1;
        this._api.pagination.countregistre=10;
        this._api.optionselection="10";
        this._api.get(this._api.pagination.page,this._api.pagination.countregistre);
        this.form.reset();
        this.idrequest=0;
      }
      else{
        this.toastr.error(response.internalErrorMessage,"Proveedores grupo");
      }
    });
  }

}
