import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormProveedoresgruposComponent } from './form-proveedoresgrupos.component';

describe('FormProveedoresgruposComponent', () => {
  let component: FormProveedoresgruposComponent;
  let fixture: ComponentFixture<FormProveedoresgruposComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormProveedoresgruposComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormProveedoresgruposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
