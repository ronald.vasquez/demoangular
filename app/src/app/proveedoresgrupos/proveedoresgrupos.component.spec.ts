import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProveedoresgruposComponent } from './proveedoresgrupos.component';

describe('ProveedoresgruposComponent', () => {
  let component: ProveedoresgruposComponent;
  let fixture: ComponentFixture<ProveedoresgruposComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProveedoresgruposComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProveedoresgruposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
