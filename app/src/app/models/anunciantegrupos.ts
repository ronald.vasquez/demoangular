import { BaseObject } from "./baseobject";

export class AnuncianteGrupos extends BaseObject{
  id?: number;
  nombre: string | undefined;
  visible: boolean | undefined;
}