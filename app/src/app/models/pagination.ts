export class Pagination{
    page: number=0;
    totalcount: number=0;
    totalpage:number=0;
    previouspage: number=0;
    showpagination: boolean=true;
    selectregistre=[10,20,30,40,50];
    countregistre=0;
    countpage=0;
}