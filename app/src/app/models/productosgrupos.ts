import { BaseObject } from "./baseobject";

export class ProductosGrupos extends BaseObject{
  id?: number;
  descripcion: string | undefined;
  visible: boolean | undefined;
}