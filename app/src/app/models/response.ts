export interface Response{
    isSuccess:boolean,
    internalErrorMessage: string,
    body: any,
    page: number;
    totalcount:number;
    totalpage:number;
}