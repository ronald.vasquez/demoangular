import { BaseObject } from "./baseobject";

export class AfiliadosGrupos extends BaseObject{
  id?: number;
  nombre: string | undefined;
  estatus: boolean | undefined;
}