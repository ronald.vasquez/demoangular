import { BaseObject } from "./baseobject";

export class ProveedoresGrupos extends BaseObject{
  id?: number;
  nombre: string | undefined;
  visible: boolean | undefined;
}