import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAfiliadosgruposComponent } from './list-afiliadosgrupos.component';

describe('ListAfiliadosgruposComponent', () => {
  let component: ListAfiliadosgruposComponent;
  let fixture: ComponentFixture<ListAfiliadosgruposComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListAfiliadosgruposComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAfiliadosgruposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
