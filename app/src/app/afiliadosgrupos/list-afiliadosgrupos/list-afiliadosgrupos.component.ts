import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AfiliadosGrupos } from 'src/app/models/afiliadosgrupos';
import { ApiafiliadosgruposService } from 'src/app/services/apiafiliadosgrupos.service';
import {NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap'; 

@Component({
  selector: 'app-list-afiliadosgrupos',
  templateUrl: './list-afiliadosgrupos.component.html',
  styleUrls: ['./list-afiliadosgrupos.component.scss'],
  providers: [NgbPaginationConfig]
})
export class ListAfiliadosgruposComponent implements OnInit {
  constructor(
    public apiresponse:ApiafiliadosgruposService,
    private config: NgbPaginationConfig,
    private toastr: ToastrService
  ) { 
    this.config.boundaryLinks = true;
  }

  ngOnInit(): void {
    this.apiresponse.pagination.page =1;
	  this.apiresponse.pagination.previouspage =1;
    this.apiresponse.pagination.countregistre=10;
    this.apiresponse.get(this.apiresponse.pagination.page,this.apiresponse.pagination.countregistre);
  }
  loadPage(page: number) {
    if (page !== this.apiresponse.pagination.previouspage) {
      this.apiresponse.pagination.previouspage = page;
      this.apiresponse.get(this.apiresponse.pagination.page,this.apiresponse.pagination.countregistre);
    }
  }
  selection(){
    this.apiresponse.pagination.countregistre =  parseInt(this.apiresponse.optionselection);
    this.apiresponse.get(this.apiresponse.pagination.page,this.apiresponse.pagination.countregistre);
  }
  update(afiliadogrupo: AfiliadosGrupos){
    this.apiresponse.update(afiliadogrupo);
  }
  delete(id?:number){
    if(confirm("Esta seguro que desea eliminar el registro")){
        this.apiresponse.delete(id).subscribe(response=>{
        if(response.isSuccess){
          this.apiresponse.pagination.page =1;
	        this.apiresponse.pagination.previouspage =1;
          this.apiresponse.pagination.countregistre=10;
          this.apiresponse.get(this.apiresponse.pagination.page,this.apiresponse.pagination.countregistre);
          this.apiresponse.optionselection="10";
        }
        else{
          this.toastr.error(response.internalErrorMessage,"Afiliado Grupo");
        }
      });
    }
  }
}
