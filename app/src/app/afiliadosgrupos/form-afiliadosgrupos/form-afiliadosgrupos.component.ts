import { Component, OnDestroy, OnInit } from '@angular/core';
import {FormGroup,FormBuilder,Validators} from '@angular/forms'
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AfiliadosGrupos } from 'src/app/models/afiliadosgrupos';
import { ApiafiliadosgruposService } from 'src/app/services/apiafiliadosgrupos.service';

@Component({
  selector: 'app-form-afiliadosgrupos',
  templateUrl: './form-afiliadosgrupos.component.html',
  styleUrls: ['./form-afiliadosgrupos.component.scss']
})
export class FormAfiliadosgruposComponent implements OnInit,OnDestroy {
  form: FormGroup;
  subscription:Subscription | undefined;
  datafgrupo:AfiliadosGrupos | undefined;
  idafigrupo:number | undefined;
  constructor(private formBuilder:FormBuilder,private _api: ApiafiliadosgruposService,
    private toastr: ToastrService) { 
    this.form=this.formBuilder.group({
      id:0,
      nombre:['',[Validators.required,Validators.maxLength(50)]],
      estatus:true
    });
  }
  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }
  ngOnInit(): void {
    this.subscription=this._api.getdata().subscribe(response=>{
      this.datafgrupo=response as AfiliadosGrupos;
      this.form.patchValue({
        nombre: this.datafgrupo.nombre,
        estatus:this.datafgrupo.estatus
      });
      this.idafigrupo=this.datafgrupo.id;
    });
  }
  save(){
    if(this.idafigrupo===undefined || this.idafigrupo===0){
      this.insert();
    }else{
      this.updateform();
    }
  }

  insert(){
    const fvalue: AfiliadosGrupos={
      nombre: this.form.get('nombre')?.value,
      estatus: this.form.get('estatus')?.value,
      iduser:0,
      codigoempresa:"001"
    }
    this._api.save(fvalue).subscribe(response=> {
      if(response.isSuccess){
        this.toastr.success("Registro agregado","Afiliado grupo");
        this._api.pagination.page =1;
	      this._api.pagination.previouspage =1;
        this._api.pagination.countregistre=10;
        this._api.optionselection="10";
        this._api.get(this._api.pagination.page,this._api.pagination.countregistre);
        this.form.reset();
      }
      else{
        this.toastr.error(response.internalErrorMessage,"Afiliado grupo");
      }
    });
  }
  updateform(){
    const fvalue: AfiliadosGrupos={
      id:this.datafgrupo?.id,
      nombre: this.form.get('nombre')?.value,
      estatus: this.form.get('estatus')?.value,
      iduser:0,
      codigoempresa:"001"
    }
    this._api.updatedata(fvalue,this.datafgrupo?.id).subscribe(response=>{
      if(response.isSuccess){
        this.toastr.info("Registro actualizado","Afiliado grupo");
        this._api.pagination.page =1;
	      this._api.pagination.previouspage =1;
        this._api.pagination.countregistre=10;
        this._api.optionselection="10";
        this._api.get(this._api.pagination.page,this._api.pagination.countregistre);
        this.form.reset();
        this.idafigrupo=0;
      }
      else{
        this.toastr.error(response.internalErrorMessage,"Afiliado grupo");
      }
    });
  }
  
}
