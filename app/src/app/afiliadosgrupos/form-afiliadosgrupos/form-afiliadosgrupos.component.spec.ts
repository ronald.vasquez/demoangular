import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAfiliadosgruposComponent } from './form-afiliadosgrupos.component';

describe('FormAfiliadosgruposComponent', () => {
  let component: FormAfiliadosgruposComponent;
  let fixture: ComponentFixture<FormAfiliadosgruposComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormAfiliadosgruposComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAfiliadosgruposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
