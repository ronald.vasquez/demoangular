import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AfiliadosgruposComponent } from './afiliadosgrupos.component';

describe('AfiliadosgruposComponent', () => {
  let component: AfiliadosgruposComponent;
  let fixture: ComponentFixture<AfiliadosgruposComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AfiliadosgruposComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AfiliadosgruposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
