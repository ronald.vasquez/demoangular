import { Component, OnDestroy, OnInit } from '@angular/core';
import { ApiafiliadosgruposService } from '../services/apiafiliadosgrupos.service';
import { Response } from '../models/response';
import { AfiliadosGrupos } from '../models/afiliadosgrupos';
@Component({
  selector: 'app-afiliadosgrupos',
  templateUrl: './afiliadosgrupos.component.html',
  styleUrls: ['./afiliadosgrupos.component.scss']
})
export class AfiliadosgruposComponent implements OnInit{
  
  
  constructor(
    private apiresponse:ApiafiliadosgruposService
  ) { 
  }
  ngOnInit(): void {
  }
}
