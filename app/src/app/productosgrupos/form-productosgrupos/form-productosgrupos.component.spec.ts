import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormProductosgruposComponent } from './form-productosgrupos.component';

describe('FormProductosgruposComponent', () => {
  let component: FormProductosgruposComponent;
  let fixture: ComponentFixture<FormProductosgruposComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormProductosgruposComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormProductosgruposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
