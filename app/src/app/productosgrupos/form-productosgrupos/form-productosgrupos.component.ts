import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { ProductosGrupos } from 'src/app/models/productosgrupos';
import { ApiproductosgruposService } from 'src/app/services/apiproductosgrupos.service';

@Component({
  selector: 'app-form-productosgrupos',
  templateUrl: './form-productosgrupos.component.html',
  styleUrls: ['./form-productosgrupos.component.scss']
})
export class FormProductosgruposComponent implements OnInit,OnDestroy {

  form: FormGroup;
  subscription:Subscription | undefined;
  datarequest:ProductosGrupos | undefined;
  idrequest:number | undefined;
  constructor(private formBuilder:FormBuilder,private _api: ApiproductosgruposService,
    private toastr: ToastrService) { 
    this.form=this.formBuilder.group({
      id:0,
      descripcion:['',[Validators.required,Validators.maxLength(50)]],
      estatus:true
    });
  }
  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.subscription=this._api.getdata().subscribe(response=>{
      this.datarequest=response as ProductosGrupos;
      this.form.patchValue({
        descripcion: this.datarequest.descripcion,
        estatus:this.datarequest.visible
      });
      this.idrequest=this.datarequest.id;
    });
  }
  save(){
    if(this.idrequest===undefined || this.idrequest===0){
      this.insert();
    }else{
      this.updateform();
    }
  }

  insert(){
    const fvalue: ProductosGrupos={
      descripcion: this.form.get('descripcion')?.value,
      visible: this.form.get('estatus')?.value,
      iduser:0,
      codigoempresa:"001"
    }
    this._api.save(fvalue).subscribe(response=> {
      if(response.isSuccess){
        this.toastr.success("Registro agregado","Productos grupo");
        this._api.pagination.page =1;
	      this._api.pagination.previouspage =1;
        this._api.pagination.countregistre=10;
        this._api.optionselection="10";
        this._api.get(this._api.pagination.page,this._api.pagination.countregistre);
        this.form.reset();
      }
      else{
        this.toastr.error(response.internalErrorMessage,"Productos grupo");
      }
    });
  }
  updateform(){
    const fvalue: ProductosGrupos={
      id:this.datarequest?.id,
      descripcion: this.form.get('descripcion')?.value,
      visible: this.form.get('estatus')?.value,
      iduser:0,
      codigoempresa:"001"
    }
    this._api.updatedata(fvalue,this.datarequest?.id).subscribe(response=>{
      if(response.isSuccess){
        this.toastr.info("Registro actualizado","Productos grupo");
        this._api.pagination.page =1;
	      this._api.pagination.previouspage =1;
        this._api.pagination.countregistre=10;
        this._api.optionselection="10";
        this._api.get(this._api.pagination.page,this._api.pagination.countregistre);
        this.form.reset();
        this.idrequest=0;
      }
      else{
        this.toastr.error(response.internalErrorMessage,"Productos grupo");
      }
    });
  }

}
