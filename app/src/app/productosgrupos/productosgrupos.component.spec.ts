import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductosgruposComponent } from './productosgrupos.component';

describe('ProductosgruposComponent', () => {
  let component: ProductosgruposComponent;
  let fixture: ComponentFixture<ProductosgruposComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductosgruposComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductosgruposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
