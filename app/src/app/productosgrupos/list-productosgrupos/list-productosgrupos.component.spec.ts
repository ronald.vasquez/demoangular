import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListProductosgruposComponent } from './list-productosgrupos.component';

describe('ListProductosgruposComponent', () => {
  let component: ListProductosgruposComponent;
  let fixture: ComponentFixture<ListProductosgruposComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListProductosgruposComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProductosgruposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
