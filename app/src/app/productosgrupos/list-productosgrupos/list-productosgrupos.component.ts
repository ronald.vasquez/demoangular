import { Component, OnInit } from '@angular/core';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ProductosGrupos } from 'src/app/models/productosgrupos';
import { ApiproductosgruposService } from 'src/app/services/apiproductosgrupos.service';

@Component({
  selector: 'app-list-productosgrupos',
  templateUrl: './list-productosgrupos.component.html',
  styleUrls: ['./list-productosgrupos.component.scss'],
  providers: [NgbPaginationConfig]
})
export class ListProductosgruposComponent implements OnInit {

  constructor(
    public apiresponse:ApiproductosgruposService,
    private config: NgbPaginationConfig,
    private toastr: ToastrService
  ) { 
    this.config.boundaryLinks = true;
  }

  ngOnInit(): void {
    this.apiresponse.pagination.page =1;
	  this.apiresponse.pagination.previouspage =1;
    this.apiresponse.pagination.countregistre=10;
    this.apiresponse.get(this.apiresponse.pagination.page,this.apiresponse.pagination.countregistre);
  }
  loadPage(page: number) {
    if (page !== this.apiresponse.pagination.previouspage) {
      this.apiresponse.pagination.previouspage = page;
      this.apiresponse.get(this.apiresponse.pagination.page,this.apiresponse.pagination.countregistre);
    }
  }
  selection(){
    this.apiresponse.pagination.countregistre =  parseInt(this.apiresponse.optionselection);
    this.apiresponse.get(this.apiresponse.pagination.page,this.apiresponse.pagination.countregistre);
  }
  update(request: ProductosGrupos){
    this.apiresponse.update(request);
  }
  delete(id?:number){
    if(confirm("Esta seguro que desea eliminar el registro")){
        this.apiresponse.delete(id).subscribe(response=>{
        if(response.isSuccess){
          this.apiresponse.pagination.page =1;
	        this.apiresponse.pagination.previouspage =1;
          this.apiresponse.pagination.countregistre=10;
          this.apiresponse.get(this.apiresponse.pagination.page,this.apiresponse.pagination.countregistre);
          this.apiresponse.optionselection="10";
        }
        else{
          this.toastr.error(response.internalErrorMessage,"Producto Grupo");
        }
      });
    }
  }

}
